/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

const verifierConfiguration = {}

const defaults = require('./defaults')

const twoGRadioButton = document.getElementById('twoGRadioButton')
const twoGPlusRadioButton = document.getElementById('twoGPlusRadioButton')
const threeGRadioButton = document.getElementById('threeGRadioButton')
const playSoundCheckbox = document.getElementById('playSoundCheckbox')

const radiobuttons = [
    twoGRadioButton,
    twoGPlusRadioButton,
    threeGRadioButton
]

let verifierConfig = { ...defaults.DEFAULT_CONFIG }
let scannedConfig = { ...defaults.DEFAULT_CONFIG }

verifierConfiguration.getConfig = function () {
    return { ...verifierConfig }
}

verifierConfiguration.getScannedConfig = function () {
    return { ...scannedConfig }
}

verifierConfiguration.extractDataFromConfigModal = function () {
    const data = { ...defaults.DEFAULT_CONFIG }

    radiobuttons.forEach(function (checkbox, index, array) {
        if (checkbox.checked) {
            data.scenario = parseInt(checkbox.value)
        }
    })

    data.play_sound = playSoundCheckbox.checked

    return data
}

verifierConfiguration.setDataInConfigModal = function (configData) {
    radiobuttons.forEach(function (checkbox, index, array) {
        checkbox.checked = configData.scenario === index
    })

    playSoundCheckbox.checked = configData.play_sound
}

verifierConfiguration.getConfigurationStrings = function (configData, addHtml = false) {
    let resultString = ''
    let prefix = '<p>'
    let suffix = '</p>\n'

    if (!addHtml) {
        prefix = ''
        suffix = '\n'
    }

    resultString = resultString + prefix + $.i18n('scenario') + ' ' + $.i18n(defaults.SCENARIO_I18N_MAPPINGS[configData.scenario]) + suffix

    const playSoundState = configData.play_sound ? 'on' : 'off'
    resultString = resultString + prefix + $.i18n('play-sound-label') + ' ' + $.i18n('play-sound-' + playSoundState) + suffix

    return resultString
}

verifierConfiguration.setConfigModalFromStoredSettings = function () {
    this.setDataInConfigModal(verifierConfig)
}

verifierConfiguration.storeScannedConfiguration = function (data) {
    const [valid, configData, errors] = this.validateConfig(data)

    // TODO: Add checks for values that should not overlap and such.

    if (valid) {
        scannedConfig = data
        console.log('Updated scanned configuration:' + data)
    } else {
        console.log('Invalid configuration:' + data)
    }

    return [valid, configData, errors]
}

verifierConfiguration.storeConfiguration = function (data) {
    const [valid, configData, errors] = this.validateConfig(data)

    // TODO: Add checks for values that should not overlap and such.

    if (valid) {
        verifierConfig = data
        console.log('Updated configuration:' + configData)
    } else {
        console.log('Invalid configuration:' + configData)
    }

    return [valid, configData, errors]
}

verifierConfiguration.setConfigFromScanned = function () {
    console.log('Updating configuration from scanned data.')
    verifierConfig = { ...scannedConfig }
}

verifierConfiguration.storeModalConfiguration = function () {
    const modalData = this.extractDataFromConfigModal()
    return this.storeConfiguration(modalData)
}

verifierConfiguration.validateConfig = function (jsonObject) {
    const valid = defaults.configSchemaValidator(jsonObject)
    return [valid, valid ? jsonObject : null, defaults.configSchemaValidator.errors]
}

verifierConfiguration.parseAndValidateConfig = function (jsonString) {
    return this.validateConfig(JSON.parse(jsonString))
}

module.exports = verifierConfiguration
