/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import baercodeParsing from './baercode_parsing'
import baercodeVerifier from './baercode_verifier'
import verifierConfiguration from './verifier_configuration'

import { jsPDF } from 'jspdf'
import defaults from './defaults'

const QRious = require('qrious')
const jsQR = require('jsqr')

// Check if key bundle is older than recommended time
if (baercodeParsing.keyBundleOlderThanRecommended()) {
    $('#keyBundleOldModal').modal('show')
}

const progressUpdateTextSpan = document.getElementById('progress-update-text-span')
// Trigger fetching of key bundle
if (typeof fetch !== 'undefined') {
    baercodeParsing.triggerKeyBundleFetchCycle(false, progressUpdateTextSpan)
}

const video = document.getElementById('vid')
const buffer = document.getElementById('buffer')
const overlay = document.getElementById('overlay')
const overlayCtx = overlay.getContext('2d')
const bufferCtx = buffer.getContext('2d')

const loadingMessage = document.getElementById('loadingMessage')

const personNameSpan = document.getElementById('person-name')
const personBirthSpan = document.getElementById('date-of-birth')
const rejectedTextSpan = document.getElementById('rejected-text-span')
const configurationScannedTextSpan = document.getElementById('scanned-configuration-text-span')

const acceptUrl = require('./sounds/accept.mp3')
const acceptSound = new Audio(acceptUrl)
const rejectUrl = require('./sounds/reject.mp3')
const rejectSound = new Audio(rejectUrl)

function triggerAccepted (personName, birthDate) {
    personNameSpan.innerText = sanitizeField(personName)
    personBirthSpan.innerText = sanitizeField(birthDate)

    if (verifierConfiguration.getConfig().play_sound) {
        acceptSound.play()
    }

    $('#acceptedModal').modal('show')
    $('#configurationModal').modal('hide')
    $('#configurationScannedModal').modal('hide')
    $('#languageChooserModal').modal('hide')
    $('#rejectedModal').modal('hide')
    $('#imprintModal').modal('hide')
    $('#scanningSpinnerModal').modal('hide')
}

function triggerRejected (text) {
    rejectedTextSpan.innerText = text

    if (verifierConfiguration.getConfig().play_sound) {
        rejectSound.play()
    }

    $('#acceptedModal').modal('hide')
    $('#configurationModal').modal('hide')
    $('#configurationScannedModal').modal('hide')
    $('#languageChooserModal').modal('hide')
    $('#rejectedModal').modal('show')
    $('#imprintModal').modal('hide')
    $('#scanningSpinnerModal').modal('hide')
}

function triggerConfiguration () {
    verifierConfiguration.setConfigModalFromStoredSettings()

    $('#acceptedModal').modal('hide')
    $('#configurationModal').modal('show')
    $('#configurationScannedModal').modal('hide')
    $('#languageChooserModal').modal('hide')
    $('#rejectedModal').modal('hide')
    $('#imprintModal').modal('hide')
    $('#scanningSpinnerModal').modal('hide')
}

function triggerLanguageChooser () {
    $('#acceptedModal').modal('hide')
    $('#configurationModal').modal('hide')
    $('#configurationScannedModal').modal('hide')
    $('#languageChooserModal').modal('show')
    $('#rejectedModal').modal('hide')
    $('#imprintModal').modal('hide')
    $('#scanningSpinnerModal').modal('hide')
}

function triggerConfigurationScanned (configurationText) {
    configurationScannedTextSpan.innerHTML = configurationText

    $('#acceptedModal').modal('hide')
    $('#configurationModal').modal('hide')
    $('#configurationScannedModal').modal('show')
    $('#languageChooserModal').modal('hide')
    $('#rejectedModal').modal('hide')
    $('#imprintModal').modal('hide')
    $('#scanningSpinnerModal').modal('hide')
}

const triggerImprint = function () {
    $('#acceptedModal').modal('hide')
    $('#configurationModal').modal('hide')
    $('#configurationScannedModal').modal('hide')
    $('#languageChooserModal').modal('hide')
    $('#rejectedModal').modal('hide')
    $('#imprintModal').modal('show')
    $('#scanningSpinnerModal').modal('hide')
}

const triggerScanningSpinner = function () {
    $('#acceptedModal').modal('hide')
    $('#configurationModal').modal('hide')
    $('#configurationScannedModal').modal('hide')
    $('#languageChooserModal').modal('hide')
    $('#rejectedModal').modal('hide')
    $('#imprintModal').modal('hide')
    $('#scanningSpinnerModal').modal('show')
}

// When the user clicks on the modal, close it
const acceptedModal = document.getElementById('acceptedModal')
acceptedModal.onclick = function () {
    $('#acceptedModal').modal('hide')
}

const rejectedModal = document.getElementById('rejectedModal')
rejectedModal.onclick = function () {
    $('#rejectedModal').modal('hide')
}

const configurationBtn = document.getElementById('configurationBtn')
configurationBtn.onclick = function () {
    triggerConfiguration()
}

const languageChooserBtns = document.getElementsByClassName('languageChooserBtn')
languageChooserBtns[0].onclick = triggerLanguageChooser
languageChooserBtns[1].onclick = triggerLanguageChooser

const imprintBtn = document.getElementById('imprintBtn')
imprintBtn.onclick = function () {
    triggerImprint()
}

const refreshKeybundle = function () {
    progressUpdateTextSpan.innerText = $.i18n('progress-update') + '0%'
    $('#keyBundleOldModal').modal('hide')
    $('#progressUpdateModal').modal('show')
    baercodeParsing.triggerKeyBundleFetchCycle(true, progressUpdateTextSpan)
}

const refreshCertificatesBtn = document.getElementById('refreshCertificatesBtn')
const refreshKeyBundleBtn = document.getElementById('refreshKeyBundleBtn')
refreshCertificatesBtn.onclick = refreshKeybundle
refreshKeyBundleBtn.onclick = refreshKeybundle

const configurationSaveBtn = document.getElementById('configurationSaveBtn')
configurationSaveBtn.onclick = function () {
    verifierConfiguration.storeModalConfiguration()
}

const configurationPdfBtn = document.getElementById('configurationPdfBtn')
configurationPdfBtn.onclick = function () {
    generateConfigPdf()
}

const configurationScannedAcceptBtn = document.getElementById('configurationScannedAcceptBtn')
configurationScannedAcceptBtn.onclick = function () {
    verifierConfiguration.setConfigFromScanned()
    $('#configurationScannedModal').modal('hide')
}

const qrCodeVerifier = new QRious({
    value: defaults.VERIFIER_LINK,
    level: 'L',
    size: 512
})

function generateConfigPdf () {
    const [valid, configData, errors] = verifierConfiguration.storeModalConfiguration() // eslint-disable-line no-unused-vars
    const qrCode = new QRious({
        value: JSON.stringify(configData),
        level: 'L',
        size: 512
    })

    const baerCodeLogo = document.getElementById('baercode_logo')
    const pdf = new jsPDF() // eslint-disable-line new-cap

    // Configuration page
    pdf.setFontSize(36)
    pdf.text($.i18n('pdf-baercode-configuration-header'), 43, 30)

    pdf.setFontSize(28)
    pdf.text($.i18n('pdf-scan-to-configure-verifier'), 43, 50)
    pdf.addImage(qrCode.toDataURL(), 'png', 43, 80, 140, 140)

    const readableConfig = verifierConfiguration.getConfigurationStrings(verifierConfiguration.getConfig())
    pdf.setFontSize(12)
    pdf.text($.i18n(readableConfig), 43, 230)

    pdf.addImage(baerCodeLogo, 'png', 130, 240, 100, 60)

    // Page linking to verifier and site
    pdf.addPage()

    pdf.setFontSize(36)
    pdf.text($.i18n('pdf-baercode-verifier-header'), 43, 30)
    pdf.setFontSize(28)
    pdf.text($.i18n('pdf-scan-to-load-verifier'), 43, 50)

    pdf.addImage(qrCodeVerifier.toDataURL(), 'png', 43, 80, 140, 140)
    pdf.text($.i18n('pdf-more-information-text'), 43, 230)

    pdf.addImage(baerCodeLogo, 'png', 130, 240, 100, 60)
    pdf.save('bärcode_verifier_configuration.pdf')
}

function drawLine (begin, end, color) {
    overlayCtx.beginPath()
    overlayCtx.moveTo(begin.x, begin.y)
    overlayCtx.lineTo(end.x, end.y)
    overlayCtx.lineWidth = 4
    overlayCtx.strokeStyle = color
    overlayCtx.stroke()
}

function aquireCamera () {
    // Use facingMode: environment to attemt to get the front camera on phones
    navigator.mediaDevices.getUserMedia({ video: { facingMode: 'environment' } }).then(function (stream) {
        video.srcObject = stream
        video.setAttribute('playsinline', true) // required to tell iOS safari we don't want fullscreen
        video.play()
        requestAnimationFrame(tick)
    })
}
let hidden, visibilityChange

if (typeof document.hidden !== 'undefined') { // Opera 12.10 and Firefox 18 and later support
    hidden = 'hidden'
    visibilityChange = 'visibilitychange'
} else if (typeof document.msHidden !== 'undefined') {
    hidden = 'msHidden'
    visibilityChange = 'msvisibilitychange'
} else if (typeof document.webkitHidden !== 'undefined') {
    hidden = 'webkitHidden'
    visibilityChange = 'webkitvisibilitychange'
}

function handleVisibilityChange () {
    if (document[hidden]) {
        video.pause()
    } else {
        aquireCamera()
    }
}

window.onload = function () {
    aquireCamera()
}

if (typeof document.addEventListener === 'undefined' || hidden === undefined) {
    console.log('A browser, such as Google Chrome or Firefox, that supports the Page Visibility API is required!')
} else {
    document.addEventListener(visibilityChange, handleVisibilityChange, false)
    video.addEventListener('pause', function () {}, false)

    video.addEventListener('play', function () {}, false)
}

function sanitizeField (str) {
    const tmp = document.createElement('div')
    tmp.textContent = str
    return tmp.innerHTML
}

async function checkData (data) {
    try {
        console.log('The data: <' + data + '>')
        // console.log(data)

        try {
            const possibleConfig = JSON.parse(data)
            const [validConfig, configData, errors] = verifierConfiguration.storeScannedConfiguration(possibleConfig) // eslint-disable-line no-unused-vars

            if (validConfig) {
                triggerConfigurationScanned(verifierConfiguration.getConfigurationStrings(configData, true))
                requestAnimationFrame(tick)
                return
            }
        } catch (e) {
            if (e instanceof SyntaxError) {
                console.log('This was not config data, continue...')
            } else {
                throw e // re-throw the error unchanged
            }
        }

        const [codeVersion, signedCbor] = baercodeParsing.base64ToVersionAndSignedCbor(data) // eslint-disable-line no-unused-vars
        const base64KeyId = baercodeParsing.getBase64KeyId(signedCbor)
        const [keyDataExists, keyDataArray] = baercodeParsing.getKeyDataFromStorage(base64KeyId)

        if (keyDataExists) {
            let validSignature = false
            let cborData = null

            let i
            for (i = 0; i < keyDataArray.length; i++) {
                [validSignature, cborData] = await baercodeParsing.validateDecodeAndDecryptSignedCborKeyData(signedCbor, keyDataArray[i])

                if (validSignature) {
                    break
                }
            }

            console.log('Signature valid? ' + validSignature)
            console.log(signedCbor)
            if (validSignature) {
                const [validBaerCode, baerCode, parseError] = baercodeVerifier.cborToBaerCode(cborData)
                const timeNow = new Date()
                const [valid, error] = baercodeVerifier.baerCodeValid(timeNow, verifierConfiguration.getConfig(), keyDataArray[i].credType, baerCode)

                if (!validBaerCode) {
                    triggerRejected($.i18n(parseError))
                } else if (valid) {
                    const fullName = baerCode.first_name.concat(' ', baerCode.last_name)
                    triggerAccepted(
                        fullName,
                        baerCode.date_of_birth.toISOString().split('T')[0],
                        $.i18n(defaults.AVAILABLE_PROCEDURES_I18N[baerCode.procedure]),
                        baerCode.procedure_operator)
                } else {
                    triggerRejected($.i18n(error))
                }
            } else {
                triggerRejected($.i18n('invalid-signature'))
            }
        } else {
            triggerRejected($.i18n('key-not-found'))
        }
    } catch (error) {
        triggerRejected($.i18n('invalid-baercode'))
        console.error(error)
    }
}

async function tick () {
    loadingMessage.innerText = $.i18n('loading-video')
    if (video.readyState === video.HAVE_ENOUGH_DATA) {
        loadingMessage.hidden = true
        overlay.height = video.videoHeight
        overlay.width = video.videoWidth
        buffer.height = video.videoHeight
        buffer.width = video.videoWidth

        bufferCtx.drawImage(video, 0, 0, buffer.width, buffer.height)
        const imageData = bufferCtx.getImageData(0, 0, buffer.width, buffer.height)
        const code = jsQR(imageData.data, imageData.width, imageData.height, {
            inversionAttempts: 'dontInvert'
        })

        if (code) {
            drawLine(code.location.topLeftCorner, code.location.topRightCorner, '#FF3B58')
            drawLine(code.location.topRightCorner, code.location.bottomRightCorner, '#FF3B58')
            drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, '#FF3B58')
            drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, '#FF3B58')

            await checkData(code.data)
        }
    }
    requestAnimationFrame(tick)
}

// The barcode scanning code comes from this gist:
// https://gist.github.com/neelbhanushali/8b77171ae7b775f2b25325760f2b5191
//
// Thank you so much Neel for saving me a lot of time! <3
//
// Author: Neel Bhanushali <neal.bhanushali@gmail.com>
document.addEventListener('keydown', function (e) {
    // add scan property to window if it does not exist
    // if (!window.hasOwnProperty('scan')) {
    if (!{}.propertyIsEnumerable.call(window, 'scan')) {
        console.log('create scan array')
        window.scan = []
    }

    // if key stroke appears after 10 ms, empty scan array
    if (window.scan.length > 0 && (e.timeStamp - window.scan.slice(-1)[0].timeStamp) > 40) {
        window.scan = []

        if ($('#registration-exams-modal').hasClass('show')) {
            $('#scanningSpinnerModal').modal('hide')
        }
    } else {
        if (!$('#registration-exams-modal').hasClass('show')) {
            triggerScanningSpinner()
        }
    }

    // if key store is enter and scan array contains keystrokes
    // dispatch `scanComplete` with keystrokes in detail property
    // empty scan array after dispatching event
    if (e.key === 'Enter' && window.scan.length > 0) {
        const scannedString = window.scan.reduce(function (scannedString, entry) {
            return scannedString + entry.key
        }, '')
        window.scan = []
        return document.dispatchEvent(new CustomEvent('scanComplete', { detail: scannedString }))
    }

    // do not listen to shift event, since key for next keystroke already contains a capital letter
    // or to be specific the letter that appears when that key is pressed with shift key
    if (e.key !== 'Shift') {
        // push `key`, `timeStamp` and calculated `timeStampDiff` to scan array
        const data = JSON.parse(JSON.stringify(e, ['key', 'timeStamp']))
        data.timeStampDiff = window.scan.length > 0 ? data.timeStamp - window.scan.slice(-1)[0].timeStamp : 0

        window.scan.push(data)
    }
})
// listen to `scanComplete` event on document
document.addEventListener('scanComplete', function (e) {
    console.log(e.detail)
    checkData(e.detail)
})
