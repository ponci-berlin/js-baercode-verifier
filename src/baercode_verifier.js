/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

const defaults = require('./defaults')
const verifierConfiguration = require('./verifier_configuration')

const baercodeVerifier = {}

Date.prototype.addDays = function (days) { // eslint-disable-line
    const date = new Date(this.valueOf())
    date.setDate(date.getDate() + days)
    return date
}

// A list of functions that will be called on the respective procedures.
// so the first procedure will be passed to the first function in this list,
// the second procedure to the second, etc.
// They all should return the following list of elements:
// - Pass: Is the procedure valid at the current step.
// - Full pass: Does the procedure satisfy the entire policy (stop processing).
// - Error: Error to propagate up.
const validationFunctions = [
    // First procedure check.
    function (procedure, profile, currentTime) {
        // If a recovery, check if it's a valid recovery.
        if (isRecovered(procedure, profile)) {
            return recoveryValid(procedure, profile, currentTime)
        }

        // If it's a vaccination, do a soft pass, as a single vax is never enough for a whole pass.
        if (isVaccine(procedure, profile)) {
            return [true, false, 'not-enough-vaccinations']
        }

        // Tests are only valid here when set to 3G (which is represented by profile.
        // single_test_valid, in case more profiles come up in the future.)
        if (isTest(procedure, profile)) {
            if (profile.single_test_valid) {
                return testValid(procedure, profile, currentTime)
            }
            return [false, false, 'procedure-not-allowed']
        }

        return [false, false, 'unknown-procedure']
    },

    // Second procedure check.
    // When we get here, we'll know the following:
    // A valid first procedure (which can only be a vaccination) has been seen.
    function (procedure, profile, currentTime) {
        // Recovery follows the same logic as always, if valid recovery, we do a full pass.
        if (isRecovered(procedure, profile)) {
            return recoveryValid(procedure, profile, currentTime)
        }

        // Vaccines are a bit more complicated, as the amount of procedures for a full pass depends
        // on the profile so we let min_procedures override fullPass.
        if (isVaccine(procedure, profile)) {
            const [vaxValid, fullPass, vaccineError] = vaccineValid(procedure, profile, currentTime, true) // eslint-disable-line no-unused-vars
            if (!vaxValid) {
                return [false, false, vaccineError]
            }

            if (profile.min_procedures <= 2 && fullPass) {
                return [true, true, null]
            } else if (!fullPass) {
                return [true, false, vaccineError]
            } else {
                return [true, false, 'not-enough-vaccinations']
            }
        }

        // Tests follow the same logic as the first procedure.
        if (isTest(procedure, profile)) {
            if (profile.single_test_valid) {
                return testValid(procedure, profile, currentTime)
            }
            return [false, false, 'procedure-not-allowed']
        }
        return [false, false, 'unknown-procedure']
    },

    // Third procedure check.
    // When we get here, we'll know the following:
    // We had 2 non-failing procedures and those were vaccines (as others are full pass).
    function (procedure, profile, currentTime) {
        // Same recovery logic as usual.
        if (isRecovered(procedure, profile)) {
            return recoveryValid(procedure, profile, currentTime)
        }

        // Check if a vaccine isn't too old, but we don't check its recency.
        if (isVaccine(procedure, profile)) {
            return vaccineValid(procedure, profile, currentTime, false)
        }

        // If we get here, we're in the 2g+ profile, so we test the test for validity.
        if (isTest(procedure, profile)) {
            return testValid(procedure, profile, currentTime)
        }
        return [false, false, 'unknown-procedure']
    }
]

function isVaccine (procedure, profile) {
    return defaults.TYPE_OF_PROCEDURE.vaccine.includes(procedure.type)
}

function isTest (procedure, profile) {
    return defaults.TYPE_OF_PROCEDURE.test.includes(procedure.type)
}

function isRecovered (procedure, profile) {
    return defaults.RECOVERED_PROCEDURE === procedure.type
}

// checks a vaccine for validity, with an optional minimum age check.
// procedure newer than current time: full fail.
// procedure too new: full fail, unless minimum age check is fails.
// procedure too old: soft pass, the next procedure can make the code valid.
// procedure in the right age range: full pass.
function vaccineValid (procedure, profile, now, checkMin) {
    const maxAge = procedure.time.addDays(profile.vaccine_max_age_days)
    const minAge = procedure.time.addDays(profile.vaccine_min_age_days)

    if (procedure.time > now) {
        return [false, false, 'procedure-time-in-future']
    } else if (checkMin && (minAge > now)) {
        return [false, false, 'procedure-age-below-minimum']
    } else if (now > maxAge) {
        return [true, false, 'procedure-age-above-maximum']
    } else {
        return [true, true, 'procedure-age-valid']
    }
}

// checks a recovered for validity
// procedure newer than current time: full fail.
// procedure too new: soft pass
// procedure too old: hard fail
// procedure in the right age range: full pass.
function recoveryValid (procedure, profile, now) {
    const maxAge = procedure.time.addDays(profile.recovery_max_age_days)
    const minAge = procedure.time.addDays(profile.recovery_min_age_days)

    if (procedure.time > now) {
        return [false, false, 'procedure-time-in-future']
    } else if (minAge > now) {
        return [true, false, 'procedure-age-below-minimum']
    } else if (now > maxAge) {
        return [false, false, 'procedure-validity-expired']
    } else if (minAge < now < maxAge) {
        return [true, true, 'recovery-age-valid']
    }

    return [false, false, 'unspecified-procedure-problem']
}

// Checks if a test is new enough.
function testValid (procedure, profile, now) {
    const maxAge = procedure.time.addDays(profile.test_max_age_days)

    if (procedure.time > now) {
        return [false, false, 'procedure-time-in-future']
    } else if (now < maxAge) {
        return [true, true, 'procedure-age-valid']
    } else {
        return [false, false, 'procedure-validity-expired']
    }
}

baercodeVerifier.validateBaerCode = function (baerCode) {
    const baerCodeCopy = { ...baerCode }
    baerCodeCopy.procedures = [...baerCode.procedures]
    baerCodeCopy.date_of_birth = baerCodeCopy.date_of_birth.toISOString()
    baerCodeCopy.procedures.forEach((element, index) => {
        baerCodeCopy.procedures[index] = {
            type: element.type,
            time: element.time.toISOString()
        }
    })
    const valid = defaults.baercodeSchemaValidator(baerCodeCopy)
    return [valid, valid ? baerCode : null, defaults.baercodeSchemaValidator.errors]
}

baercodeVerifier.cborToBaerCode = function (cborData) {
    const procedures = cborData[4].sort((a, b) => b[1] - a[1]).map(p => {
        const procedure = {
            type: p[0],
            time: new Date(p[1] * 1000)
        }
        return procedure
    }
    )

    const baercode = {
        first_name: cborData[0],
        last_name: cborData[1],
        date_of_birth: new Date(cborData[2] * 1000),
        disease_type: cborData[3],
        procedures: procedures,
        procedure_operator: cborData[5],
        procedure_result: cborData[6]
    }

    return this.validateBaerCode(baercode)
}

baercodeVerifier.parseAndValidateBaerCode = function (jsonString) {
    return this.validateBaerCode(JSON.parse(jsonString))
}

baercodeVerifier.checkConfigAndBaerCode = function (configuration, baerCode) {
    const [validConfig, processedConfig, configErrors] = verifierConfiguration.validateConfig(configuration) // eslint-disable-line no-unused-vars
    const [validBaerCode, processedBaerCode, baerCodeErrors] = this.validateBaerCode(baerCode) // eslint-disable-line no-unused-vars
    const configAndBaerCode = {
        config: processedConfig,
        baercode: processedBaerCode
    }

    if (!validConfig) {
        return [false, configAndBaerCode, 'invalid-configuration']
    }

    if (!validBaerCode) {
        return [false, configAndBaerCode, 'invalid-baercode']
    }

    return [true, configAndBaerCode, null]
}

baercodeVerifier.baerCodeValid = function (currentTime, configuration, credType, baerCode) {
    let [valid, configAndBaerCode, error] = this.checkConfigAndBaerCode(configuration, baerCode)
    if (!valid) {
        return [valid, error]
    }

    const profile = defaults.PROFILES[configAndBaerCode.config.scenario]

    const procedures = configAndBaerCode.baercode.procedures
    procedures.sort((a, b) => a.time.getTime() - b.time.getTime())

    // Just eliminate any code that has procedures in the future.
    if (procedures[procedures.length - 1].time > currentTime) {
        return [false, 'procedure-time-in-future']
    }

    let last = false;
    [valid, error] = this.authorisedCredType(procedures, credType)
    if (!valid) {
        return [valid, error]
    }

    // We loop over our validation functions.
    for (let i = 0; i < validationFunctions.length; i++) {
        const f = validationFunctions[i]
        // If the loop hasn't ended yet, and we're out of procedures, return
        // with a failure and the last error.
        if (procedures[i] === undefined) {
            return [false, error]
        }

        // Run the validation function with the corresponding procedure.
        [valid, last, error] = f(procedures[i], profile, currentTime)

        // If not valid, return the error, this is a hard fail.
        if (!valid) {
            return [valid, error]
        }

        // if last is true, we break out of the loop, essentially a hard pass.
        if (last) {
            break
        }
    }

    // If we haven't gotten a hard pass, we bail for safety.
    if (!last) {
        return [false, error]
    }

    // If the hard pass came from a recovery, 2G+ requires a valid test.
    if (error === 'recovery-age-valid' && profile.recovery_test_required) {
        const proc = procedures.pop()
        valid = false
        error = 'no-test-after-recovery'
        if (isTest(proc, profile)) {
            [valid, last, error] = testValid(proc, profile, currentTime)
        }
        if (!valid) {
            return [valid, error]
        }
    }

    return [true, error]
}

baercodeVerifier.authorisedCredType = function (procedures, credType) {
    if (!Object.keys(defaults.CREDENTIAL_TYPE).includes(credType.toString())) {
        return [false, 'credential-type-not-found']
    }

    const credentialType = defaults.CREDENTIAL_TYPE[credType]
    const procedureTypes = defaults.ALLOWED_PROCEDURES[credentialType]

    for (let i = 0; i < procedures.length; i++) {
        if (!procedureTypes.includes(procedures[i].type)) {
            return [false, 'credential-type-mismatch']
        }
    }

    return [true, 'credential-type-matches']
}

module.exports = baercodeVerifier
