# js-BärCODE-verifier

A browser app for verifying BärCODEs.

## BärCODE validity

For a valid bärcode, the entire row needs to be true.

Note: See procedure types below.

```
| Credential Type | Procedure 1                      | Procedure 2 | Procedure 3 | Min age of most recent procedure | Max age of most recent procedure
| 2               | Any test                         | --          | ..          | 0                                | 24h
| 1               | Recovered                        | --          | ..          | 28d                              | 180d
| 1               | Janssen vaccine                  | --          | ..          | 14d                              | 6m
| 1               | Any vaccine                      | Any vaccine | Any vaccine | 14d                              | 6m
| 1               | Recovered                        | Any vaccine | Any vaccine | 14d                              | 6m
| 1               | Boostered (Any vaccine except 8) | Any vaccine | Any vaccine | 0d                               | 6m
| 1               | Test                             | Any vaccine | Any vaccine | 0d                               | 24h
```

## List of Vaccines

See [credential.go](https://gitlab.com/ponci-berlin/ponci/-/blob/main/pkg/credential/credential.go) for the implementation. Here is a tabular overview of the procedures:

```
| Procedure ID | Meaning           |
| 0            | ./.               |
| 1            | Antigen Quicktest |
| 2            | PCR Test          |
| 3            | Cormirnaty - BioNTech Manufacturing GmbH - mRNA vaccine - Dual shots required.                   |
| 4            | Janssen - Janssen-Cilag International NV - Vector vaccine - Single shot required.                |
| 5            | Moderna - Moderna Biotech Spain, S.L. - mRNA vaccine - Dual shots required.                      |
| 6            | Vaxzevria - AstraZeneca AB, Sweden - Vector vaccine - Dual shots required.                       |   
| 7            | TestResult - For positive diagnosis for recovered certificates (usually a positive PCR test).    |
| 8            | Nuvaxovid - Novavax, Inc. - Protein based vaccine - Dual shots required.
|              | Can be combined with any vaccination.                                                            |
```

## Format of a BärCODE

See the [BärCODE Specification](https://gitlab.com/ponci-berlin/ponci/-/blob/main/docs/spec.md) for a structured
description of the BärCODE format.

## Building, running and testing

### Requirements

*  Go version 1.16.4 or later is required for the wasm module.

### Before running or building

* `npm install`
* `npm run build-wasm`

### Run application

* `npm run start`
* Open browser and go to http://localhost:1234

### Building application

* `npm run build`
* Copy contents to root of $WEBSERVER virtual host

### Run tests

Note: The integration tests require generated test data from other repositories, so this is maybe not useful for you.

* `npm run test`

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.
