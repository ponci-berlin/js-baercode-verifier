/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require('jsdom-global')()
const assert = require('assert')
const defaults = require('../src/defaults')
const baercodeVerifier = require('../src/baercode_verifier')

const DEFAULT_CONFIG = { ...defaults.DEFAULT_CONFIG }

Date.prototype.addDays = function (days) { // eslint-disable-line
    const date = new Date(this.valueOf())
    date.setDate(date.getDate() + days)
    return date
}

Date.prototype.subDays = function (days) { // eslint-disable-line
    const date = new Date(this.valueOf())
    date.setDate(date.getDate() - days)
    return date
}

Date.prototype.addSeconds = function (seconds) { // eslint-disable-line
    const date = new Date(this.valueOf())
    date.setSeconds(date.getSeconds() + seconds)
    return date
}

Date.prototype.subSeconds = function (seconds) { // eslint-disable-line
    const date = new Date(this.valueOf())
    date.setSeconds(date.getSeconds() - seconds)
    return date
}

const TIME_NOW = new Date('2022-02-22 22:22:00 GMT')

const defaultBaercode = function () {
    return {
        first_name: 'Max',
        last_name: 'Mustermann',
        date_of_birth: new Date('1990-01-02'),
        disease_type: 1,
        procedures: [
            {
                type: 4,
                time: new Date('1977-09-05')
            },
            {
                type: 4,
                time: new Date('1977-10-05')
            }
        ],
        procedure_operator: 'Acme GmbH',
        procedure_result: false
    }
}

const validationTests = [
    {
        name: 'non-existent procedure',
        credentialType: 1,
        procedures: [
            {
                type: 65535,
                time: TIME_NOW.addDays(30).subSeconds(10)
            }
        ],
        threeG: {
            valid: false,
            message: 'invalid-baercode'
        },
        twoG: {
            valid: false,
            message: 'invalid-baercode'
        },
        twoGPlus: {
            valid: false,
            message: 'invalid-baercode'
        }
    },
    {
        name: 'non-matching procedureType',
        credentialType: 2,
        procedures: [
            {
                type: 6,
                time: TIME_NOW.subDays(30).subSeconds(10)
            }
        ],
        threeG: {
            valid: false,
            message: 'credential-type-mismatch'
        },
        twoG: {
            valid: false,
            message: 'credential-type-mismatch'
        },
        twoGPlus: {
            valid: false,
            message: 'credential-type-mismatch'
        }
    },
    {
        name: 'antigen tested 10 seconds in the future',
        credentialType: 2,
        procedures: [
            {
                type: 1,
                time: TIME_NOW.addSeconds(10)
            }
        ],
        threeG: {
            valid: false,
            message: 'procedure-time-in-future'
        },
        twoG: {
            valid: false,
            message: 'procedure-time-in-future'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-time-in-future'
        }
    },
    {
        name: 'antigen tested 23 hours and 50 seconds ago',
        credentialType: 2,
        procedures: [
            {
                type: 1,
                time: TIME_NOW.subDays(1).addSeconds(10)
            }
        ],
        threeG: {
            valid: true,
            message: 'procedure-age-valid'
        },
        twoG: {
            valid: false,
            message: 'procedure-not-allowed'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-not-allowed'
        }
    },
    {
        name: 'antigen tested 24 hours and 10 seconds ago',
        credentialType: 2,
        procedures: [
            {
                type: 1,
                time: TIME_NOW.subDays(1).subSeconds(10)
            }
        ],
        threeG: {
            valid: false,
            message: 'procedure-validity-expired'
        },
        twoG: {
            valid: false,
            message: 'procedure-not-allowed'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-not-allowed'
        }
    },
    {
        name: 'PCR tested 23 hours and 50 seconds ago',
        credentialType: 2,
        procedures: [
            {
                type: 2,
                time: TIME_NOW.subDays(1).addSeconds(10)
            }
        ],
        threeG: {
            valid: true,
            message: 'procedure-age-valid'
        },
        twoG: {
            valid: false,
            message: 'procedure-not-allowed'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-not-allowed'
        }
    },
    {
        name: 'PCR tested 24 hours and 10 seconds ago',
        credentialType: 2,
        procedures: [
            {
                type: 2,
                time: TIME_NOW.subDays(1).subSeconds(10)
            }
        ],
        threeG: {
            valid: false,
            message: 'procedure-validity-expired'
        },
        twoG: {
            valid: false,
            message: 'procedure-not-allowed'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-not-allowed'
        }
    },
    {
        name: 'single vaccinated 30 days ago',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(30)
            }
        ],
        threeG: {
            valid: false,
            message: 'not-enough-vaccinations'
        },
        twoG: {
            valid: false,
            message: 'not-enough-vaccinations'
        },
        twoGPlus: {
            valid: false,
            message: 'not-enough-vaccinations'
        }
    },
    {
        name: 'double vaccinated 30 days ago',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(60)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(30)
            }
        ],
        threeG: {
            valid: true,
            message: null
        },
        twoG: {
            valid: true,
            message: null
        },
        twoGPlus: {
            valid: false,
            message: 'not-enough-vaccinations'
        }
    },
    {
        name: 'double vaccinated 13 days ago',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(60)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(13)
            }
        ],
        threeG: {
            valid: false,
            message: 'procedure-age-below-minimum'
        },
        twoG: {
            valid: false,
            message: 'procedure-age-below-minimum'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-age-below-minimum'
        }
    },
    {
        name: 'double vaccinated 1 day from now',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(40)
            },
            {
                type: 4,
                time: TIME_NOW.addDays(1)
            }
        ],
        threeG: {
            valid: false,
            message: 'procedure-time-in-future'
        },
        twoG: {
            valid: false,
            message: 'procedure-time-in-future'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-time-in-future'
        }
    },
    {
        name: 'double vaccinated 181 days ago',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(200)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(181)
            }
        ],
        threeG: {
            valid: false,
            message: 'procedure-age-above-maximum'
        },
        twoG: {
            valid: false,
            message: 'procedure-age-above-maximum'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-age-above-maximum'
        }
    },
    {
        name: 'triple vaccinated 30 days ago',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(181)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(30)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(60)
            }
        ],
        threeG: {
            valid: true,
            message: null
        },
        twoG: {
            valid: true,
            message: null
        },
        twoGPlus: {
            valid: true,
            message: 'procedure-age-valid'
        }
    },
    {
        name: 'triple vaccinated 181 days ago',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(210)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(181)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(240)
            }
        ],
        threeG: {
            valid: false,
            message: 'procedure-age-above-maximum'
        },
        twoG: {
            valid: false,
            message: 'procedure-age-above-maximum'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-age-above-maximum'
        }
    },
    {
        name: 'triple vaccinated quick succession',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(3)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(2)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(1)
            }
        ],
        threeG: {
            valid: false,
            message: 'procedure-age-below-minimum'
        },
        twoG: {
            valid: false,
            message: 'procedure-age-below-minimum'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-age-below-minimum'
        }
    },
    {
        name: 'triple vaccinated 1 day from now',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(70)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(40)
            },
            {
                type: 4,
                time: TIME_NOW.addDays(1)
            }
        ],
        threeG: {
            valid: false,
            message: 'procedure-time-in-future'
        },
        twoG: {
            valid: false,
            message: 'procedure-time-in-future'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-time-in-future'
        }
    },
    {
        name: 'double vaccinated 179 days ago + 1 hour old test test',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(200)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(179)
            },
            {
                type: 1,
                time: TIME_NOW.subSeconds(3600)
            }
        ],
        threeG: {
            valid: true,
            message: null
        },
        twoG: {
            valid: true,
            message: null
        },
        twoGPlus: {
            valid: true,
            message: 'procedure-age-valid'
        }
    },
    {
        name: 'double vaccinated 179 days ago + 24h10s test',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(200)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(179)
            },
            {
                type: 1,
                time: TIME_NOW.subDays(1).subSeconds(10)
            }
        ],
        threeG: {
            valid: true,
            message: null
        },
        twoG: {
            valid: true,
            message: null
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-validity-expired'
        }
    },
    {
        name: 'double vaccinated 181 days ago + 24h10s test',
        credentialType: 1,
        procedures: [
            {
                type: 4,
                time: TIME_NOW.subDays(200)
            },
            {
                type: 4,
                time: TIME_NOW.subDays(181)
            },
            {
                type: 1,
                time: TIME_NOW.subDays(1).subSeconds(10)
            }
        ],
        threeG: {
            valid: false,
            message: 'procedure-validity-expired'
        },
        twoG: {
            valid: false,
            message: 'procedure-validity-expired'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-validity-expired'
        }
    },
    {
        name: 'recovered 27 days ago',
        credentialType: 1,
        procedures: [
            {
                type: 7,
                time: TIME_NOW.subDays(27)
            }
        ],
        threeG: {
            valid: false,
            message: 'procedure-age-below-minimum'
        },
        twoG: {
            valid: false,
            message: 'procedure-age-below-minimum'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-age-below-minimum'
        }
    },
    {
        name: 'recovered 28 days ago',
        credentialType: 1,
        procedures: [
            {
                type: 7,
                time: TIME_NOW.subDays(28)
            }
        ],
        threeG: {
            valid: true,
            message: 'recovery-age-valid'
        },
        twoG: {
            valid: true,
            message: 'recovery-age-valid'
        },
        twoGPlus: {
            valid: false,
            message: 'no-test-after-recovery'
        }
    },
    {
        name: 'recovered 27 days ago and valid test',
        credentialType: 1,
        procedures: [
            {
                type: 7,
                time: TIME_NOW.subDays(27)
            },
            {
                type: 1,
                time: TIME_NOW.subDays(1).addSeconds(10)
            }
        ],
        threeG: {
            valid: true,
            message: 'procedure-age-valid'
        },
        twoG: {
            valid: false,
            message: 'procedure-not-allowed'
        },
        twoGPlus: {
            valid: false,
            message: 'procedure-not-allowed'
        }
    },
    {
        name: 'recovered 28 days ago and valid test',
        credentialType: 1,
        procedures: [
            {
                type: 7,
                time: TIME_NOW.subDays(28)
            },
            {
                type: 1,
                time: TIME_NOW.subDays(1).addSeconds(10)
            }
        ],
        threeG: {
            valid: true,
            message: 'recovery-age-valid'
        },
        twoG: {
            valid: true,
            message: 'recovery-age-valid'
        },
        twoGPlus: {
            valid: true,
            message: 'procedure-age-valid'
        }
    }
]

describe('codeVerification', function () {
    describe('3G', function () {
        const CONFIG_THREEG = { ...DEFAULT_CONFIG }
        CONFIG_THREEG.scenario = 2

        validationTests.forEach((element) => {
            it(element.name, function () {
                const baerCode = defaultBaercode()
                baerCode.procedures = element.procedures
                const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, CONFIG_THREEG, element.credentialType, baerCode)
                assert.strictEqual(message, element.threeG.message)
                assert.strictEqual(valid, element.threeG.valid)
            })
        })
    })

    describe('2G', function () {
        const CONFIG_TWOG = { ...DEFAULT_CONFIG }
        CONFIG_TWOG.scenario = 0

        validationTests.forEach((element) => {
            it(element.name, function () {
                const baerCode = defaultBaercode()
                baerCode.procedures = element.procedures
                const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, CONFIG_TWOG, element.credentialType, baerCode)
                assert.strictEqual(message, element.twoG.message)
                assert.strictEqual(valid, element.twoG.valid)
            })
        })
    })

    describe('2G+', function () {
        const CONFIG_TWOGPLUS = { ...DEFAULT_CONFIG }
        CONFIG_TWOGPLUS.scenario = 1

        validationTests.forEach((element) => {
            it(element.name, function () {
                const baerCode = defaultBaercode()
                baerCode.procedures = element.procedures
                const [valid, message] = baercodeVerifier.baerCodeValid(TIME_NOW, CONFIG_TWOGPLUS, element.credentialType, baerCode)
                assert.strictEqual(message, element.twoGPlus.message)
                assert.strictEqual(valid, element.twoGPlus.valid)
            })
        })
    })
})
