/*
Copyright 2021 The PONCi Berlin Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

require('jsdom-global')()

const assert = require('assert')
const verifierConfiguration = require('../src/verifier_configuration')
describe('Verifier configuration', function () {
    describe('parseAndValidateConfig()', function () {
        it('should return a config object when passed proper json string configuration', function () {
            const testData = {
                scenario: 1,
                play_sound: false
            }
            const jsonString = JSON.stringify(testData)

            const [valid, config, errors] = verifierConfiguration.parseAndValidateConfig(jsonString)
            assert.strictEqual(valid, true)
            assert.deepStrictEqual(config, testData)
            assert.strictEqual(errors, null)
        })
    })

    describe('validateConfig()', function () {
        const testData = {
            scenario: 0,
            play_sound: false
        }

        it('should return a config object when passed proper configuration', function () {
            const [valid, config, errors] = verifierConfiguration.validateConfig(testData)
            assert.strictEqual(valid, true)
            assert.deepStrictEqual(config, testData)
            assert.strictEqual(errors, null)
        })

        it('should return an error saying required items are missing', function () {
            for (const [key, value] of Object.entries(testData)) { // eslint-disable-line no-unused-vars
                const data = { ...testData }
                delete data[key]
                const [valid, config, errors] = verifierConfiguration.validateConfig(data)
                assert.strictEqual(valid, false)
                assert.deepStrictEqual(config, null)
                assert.strictEqual(errors[0].keyword, 'required')
            }
        })

        it('should return an error saying what is wrong with the value', function () {
            const testexpectations = {
                scenario: [{
                    value: -1,
                    keyword: 'minimum',
                    message: 'should be >= 0'
                },
                {
                    value: 3,
                    keyword: 'maximum',
                    message: 'should be <= 2'
                }
                ]
            }

            for (const [configName, testList] of Object.entries(testexpectations)) {
                testList.forEach(function (testInput, index, array) {
                    const data = { ...testData }
                    data[configName] = testInput.value
                    const [valid, config, errors] = verifierConfiguration.validateConfig(data) // eslint-disable-line no-unused-vars
                    assert.strictEqual(errors[0].keyword, testInput.keyword)
                    assert.strictEqual(errors[0].message, testInput.message)
                })
            }
        })
    })
})
